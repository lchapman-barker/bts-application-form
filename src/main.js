import Vue from 'vue';
import App from './App.vue';
import router from './router';

// VeeValidate
import './vee-validate';

// == CSS Framework
import Buefy from 'buefy';
// import 'buefy/dist/buefy.css';
import './assets/style.scss';
import '@mdi/font/css/materialdesignicons.css';

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
	router,
	render: (h) => h(App),
}).$mount('#app');
