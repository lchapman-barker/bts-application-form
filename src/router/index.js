import Vue from 'vue';
import VueRouter from 'vue-router';
import ApplicationForm from '../views/ApplicationForm.vue';
import Confirmation from '../views/Confirmation.vue';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'ApplicationForm',
		component: ApplicationForm,
	},
	{
		path: '/confirmation',
		name: 'Confirmation',
		component: Confirmation,
	},
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
