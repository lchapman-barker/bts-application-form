// VeeValidate is a form validation package, see: https://logaretm.github.io/vee-validate/
import { required, email, numeric, min, max } from 'vee-validate/dist/rules';
import { extend } from 'vee-validate';

extend('required', {
	...required,
	message: `The {_field_} field is required`,
});

extend('email', {
	...email,
	message: `The {_field_} field must be a valid email address`,
});

extend('numeric', {
	...numeric,
	message: `The {_field_} field should only contain numbers`,
});

extend('min', {
	...min,
	message: `The {_field_} field should be atleast {length} characters long`,
});

extend('max', {
	...max,
	message: `The {_field_} field should not exceed {length} characters`,
});
